

#ifndef L4END_FRAMING_H
#define L4END_FRAMING_H

#include <string>
#include <vector>
#include <functional>

namespace l4end
{

union t_len1s {
	char _ach4[4];
	uint32_t _ui32;
};

std::string length_1s(const std::string& str_msg);

std::string enframe(const std::string& str_msg);
std::size_t deframe(std::vector<char>* pvch_buf, const char* kpch_recvd, std::size_t szt_length, std::function<void(std::string)> lf_process_msg);

}
	
#endif // L4END_FRAMING_H
