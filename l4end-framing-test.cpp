
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "l4end-framing.h"

const std::vector<char> kvch_F00D_MARKER= { (char)0xF0, (char)0x0D };
const std::vector<char> kvch_DEAD_MARKER= { (char)0xDE, (char)0xAD };

// https://stackoverflow.com/a/16125797/298545	
static std::string string_to_hex(const std::string& in) {
    std::stringstream ss;

    ss << std::hex << std::setfill('0');
    for (size_t i = 0; in.length() > i; ++i) {
        ss << std::setw(2) << static_cast<unsigned int>(static_cast<unsigned char>(in[i]));
    }

    return ss.str(); 
}

void l4end_framing_test()
{
	// enframe

	{
		std::string str_msg= "";
		std::string str_out= l4end::enframe(str_msg);
		std::cout << "l4end::enframe(""): [" << string_to_hex(str_out) << "]" << std::endl;
	}

	{
		std::string str_msg= "02o4wgwtmmvsziv8hxmo4gn9uqpcgl4bjnlho1dxryzs7nc7tmrpkn1ql2tgvhzl0rk8vtqbcjhjn8e44imhzqz3jpjgc3y9ue43rhzvj7vp2mhglgzrxugtuwjbjq6zjss9d92k0kl8av5t020e4l4osvc49jljdzeciul7etdjukkty8rhiuo6x7gprcno15a39d72tctgk7s2azswz4o10a5c0c2dmluy0vk4gm4h1mydh9h2jecl9dmx0ouldij45k8bnzng70l3ceabzomyjibm5k2uxhj9neg27pv053ixh0mu4igc5qzimcvn";
		std::string str_out= l4end::enframe(str_msg);

		std::cout << "l4end::enframe(02o4...mcvn): [" << string_to_hex(str_out) << "]" << std::endl;
	}

	// deframe

	std::vector<char> vch_buffer;

	vch_buffer.clear();
	{
		std::cout << "! (pos != pvch_buf->end())";

		std::string str_in;
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		// std::cout << "[" << string_to_hex(str_in) << "]" << std::endl;

		// should break out of the function awaiting more data
		std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
				assert(false);
			});		

		assert(sz == 0);
		std::cout << " == OK" << std::endl;
	}

	vch_buffer.clear();
	{
		std::cout << "! (pvch_buf->size() >= kszt_MARKER_SIZE + kszt_LENGTH_FIELD_SIZE + kszt_MARKER_SIZE)";

		std::string str_in;
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_F00D_MARKER.begin(), kvch_F00D_MARKER.end());
		str_in+= l4end::length_1s(str_in);
		// std::cout << "[" << string_to_hex(str_in) << "]" << std::endl;

		// should break out of the function awaiting more data
		std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
				assert(false);
			});		

		assert(sz == 6);
		std::cout << " == OK" << std::endl;
	}

	// TODO: ! (kvch_F00D != kvch_F00D_MARKER)

	vch_buffer.clear();
	{
		std::cout << "! (ui_msglen >= kszt_MAX_MSG_SIZE)";

		std::string str_in;
		str_in.insert(str_in.end(), kvch_F00D_MARKER.begin(), kvch_F00D_MARKER.end());

		// 1's compliment, high left/right, low left/right
		l4end::t_len1s len1s;
		len1s._ui32= ~ 0x100000;
		// printf("ui_len 1s: %x: %x %x %x %x \n", len1s._ui32, len1s._ach4[3]&0xff, len1s._ach4[2]&0xff, len1s._ach4[1]&0xff, len1s._ach4[0]&0xff );
		str_in+= std::string( len1s._ach4, 4);
		str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
		str_in.insert(str_in.end(), kvch_F00D_MARKER.begin(), kvch_F00D_MARKER.end());
		// std::cout << "[" << string_to_hex(str_in) << "]" << std::endl;

		std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
				assert(false);
			});

		assert(sz == 2);
		std::cout << " == OK" << std::endl;
	}

	vch_buffer.clear();
	{
		std::cout << "! (pvch_buf->size() < kszt_LENGTH_FRAME)";

		std::string str_msg= "        ";
		std::string str_in= l4end::enframe(str_msg);
		str_in.erase(str_in.end()-2, str_in.end());
		// std::cout << "[" << string_to_hex(str_in) << "]" << std::endl;

		std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
				assert(false);
			});

		assert(sz == str_in.size());
		std::cout << " == OK" << std::endl;
	}

	// DON'T: vch_buffer.clear();
	{
		std::cout << "Wait for more data ...";

		std::string str_in;
		str_in.insert(str_in.end(), kvch_F00D_MARKER.begin(), kvch_F00D_MARKER.end());
		// std::cout << "[" << string_to_hex(str_in) << "]" << std::endl;

		std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
				assert(false);
			});

		assert(sz == 2);
		std::cout << " == OK" << std::endl;
	}

	vch_buffer.clear();
	{
		std::cout << "l4end::deframe(...) == \"02o4...mcvn\" ";

		std::string str_msg= "02o4wgwtmmvsziv8hxmo4gn9uqpcgl4bjnlho1dxryzs7nc7tmrpkn1ql2tgvhzl0rk8vtqbcjhjn8e44imhzqz3jpjgc3y9ue43rhzvj7vp2mhglgzrxugtuwjbjq6zjss9d92k0kl8av5t020e4l4osvc49jljdzeciul7etdjukkty8rhiuo6x7gprcno15a39d72tctgk7s2azswz4o10a5c0c2dmluy0vk4gm4h1mydh9h2jecl9dmx0ouldij45k8bnzng70l3ceabzomyjibm5k2uxhj9neg27pv053ixh0mu4igc5qzimcvn";
		std::string str_in= l4end::enframe(str_msg);
		// std::cout << "[" << string_to_hex(str_in) << "]" << std::endl;

		std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
				// std::cout << "l4end::deframe [" << str_msg << "]" << std::endl;
				assert(0 == str_msg.compare("02o4wgwtmmvsziv8hxmo4gn9uqpcgl4bjnlho1dxryzs7nc7tmrpkn1ql2tgvhzl0rk8vtqbcjhjn8e44imhzqz3jpjgc3y9ue43rhzvj7vp2mhglgzrxugtuwjbjq6zjss9d92k0kl8av5t020e4l4osvc49jljdzeciul7etdjukkty8rhiuo6x7gprcno15a39d72tctgk7s2azswz4o10a5c0c2dmluy0vk4gm4h1mydh9h2jecl9dmx0ouldij45k8bnzng70l3ceabzomyjibm5k2uxhj9neg27pv053ixh0mu4igc5qzimcvn"));
			});

		assert(sz == 0);
		std::cout << " == OK" << std::endl;
	}

	vch_buffer.clear();
	{
		std::cout << "COMBINED";

		{
			// DEAD
			//
			std::string str_in;
			str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
			str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
			str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
			str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
			str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
			str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());
			str_in.insert(str_in.end(), kvch_F00D_MARKER.begin(), kvch_F00D_MARKER.end());

			// F0A1
			//
			const std::vector<char> kvch_F0A1_MARKER= { (char)0xF0, (char)0xAI };
			str_in.insert(str_in.end(), kvch_F0A1_MARKER.begin(), kvch_F0A1_MARKER.end());

			// ! (ui_msglen >= kszt_MAX_MSG_SIZE)
			//
			str_in.insert(str_in.end(), kvch_F00D_MARKER.begin(), kvch_F00D_MARKER.end());

			// 1's compliment, high left/right, low left/right
			l4end::t_len1s len1s;
			len1s._ui32= ~ 0x100000;
			// printf("ui_len 1s: %x: %x %x %x %x \n", len1s._ui32, len1s._ach4[3]&0xff, len1s._ach4[2]&0xff, len1s._ach4[1]&0xff, len1s._ach4[0]&0xff );
			str_in+= std::string( len1s._ach4, 4);

			// ! (pvch_buf->size() < kszt_LENGTH_FRAME)
			//
			std::string str_msg= "        ";
			str_in+= l4end::enframe(str_msg);
			str_in.erase(str_in.end()-2, str_in.end());

			std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
					assert(false);
				});

			assert(sz == 14);
		}

		{
			//  02o4...mcvn
			//

			std::string str_in;
			str_in.insert(str_in.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());

			std::string str_msg= "02o4wgwtmmvsziv8hxmo4gn9uqpcgl4bjnlho1dxryzs7nc7tmrpkn1ql2tgvhzl0rk8vtqbcjhjn8e44imhzqz3jpjgc3y9ue43rhzvj7vp2mhglgzrxugtuwjbjq6zjss9d92k0kl8av5t020e4l4osvc49jljdzeciul7etdjukkty8rhiuo6x7gprcno15a39d72tctgk7s2azswz4o10a5c0c2dmluy0vk4gm4h1mydh9h2jecl9dmx0ouldij45k8bnzng70l3ceabzomyjibm5k2uxhj9neg27pv053ixh0mu4igc5qzimcvn";
			str_in+= l4end::enframe(str_msg);

			std::size_t sz= l4end::deframe(&vch_buffer, str_in.c_str(), str_in.size(), [](std::string str_msg) {
					// std::cout << "l4end::deframe [" << str_msg << "]" << std::endl;
					assert(!str_msg.compare("        ") || !str_msg.compare("02o4wgwtmmvsziv8hxmo4gn9uqpcgl4bjnlho1dxryzs7nc7tmrpkn1ql2tgvhzl0rk8vtqbcjhjn8e44imhzqz3jpjgc3y9ue43rhzvj7vp2mhglgzrxugtuwjbjq6zjss9d92k0kl8av5t020e4l4osvc49jljdzeciul7etdjukkty8rhiuo6x7gprcno15a39d72tctgk7s2azswz4o10a5c0c2dmluy0vk4gm4h1mydh9h2jecl9dmx0ouldij45k8bnzng70l3ceabzomyjibm5k2uxhj9neg27pv053ixh0mu4igc5qzimcvn"));
				});

			assert(sz == 0);
		}

		std::cout << " == OK" << std::endl;
	}

}
