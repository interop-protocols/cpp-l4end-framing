//
//  l4end-framing.cpp
//
//  Created by dev on 2023-06-13.
//

#include "l4end-framing.h"

#include <assert.h>
#include <cstring>
#include <iostream>
#include <vector>
#include <sstream>
#include <iomanip>

const std::size_t kszt_LENGTH_FIELD_SIZE= 4;
const std::size_t kszt_MAX_MSG_SIZE= 0xFFFFF;
const std::size_t kszt_MAX_BUFFER_SIZE= 0xFFFFFF;

const std::size_t kszt_MARKER_SIZE= 2;
const std::vector<char> kvch_F00D_MARKER= { (char)0xF0, (char)0x0D };
const std::vector<char> kvch_DEAD_MARKER= { (char)0xDE, (char)0xAD };

//----------------------------------------------------------------------------------------------------------------

// https://stackoverflow.com/a/16125797/298545	
// static std::string string_to_hex(const std::string& in) {
//     std::stringstream ss;

//     ss << std::hex << std::setfill('0');
//     for (size_t i = 0; in.length() > i; ++i) {
//         ss << std::setw(2) << static_cast<unsigned int>(static_cast<unsigned char>(in[i]));
//     }

//     return ss.str(); 
// }

std::string l4end::length_1s(const std::string& str_msg) 
{
	assert(str_msg.length() < kszt_MAX_MSG_SIZE);
	if (str_msg.length() >= kszt_MAX_MSG_SIZE)
		throw kszt_MAX_MSG_SIZE;

	t_len1s len1s_1234;
	len1s_1234._ui32= 0x01020304;

	assert(len1s_1234._ach4[0] == 4); // check endianess
	assert(sizeof(int) == kszt_LENGTH_FIELD_SIZE);

	// 1's compliment, high left/right, low left/right
	t_len1s len1s;
	len1s._ui32= ~ (unsigned int) str_msg.length();

	// printf("ui_len 1s: %x: %x %x %x %x \n", len1s._ui32, len1s._ach4[3]&0xff, len1s._ach4[2]&0xff, len1s._ach4[1]&0xff, len1s._ach4[0]&0xff );

	return std::string( len1s._ach4, 4);
}

std::string l4end::enframe(const std::string& str_msg) 
{
	std::string str_len= length_1s(str_msg);

	// NSData* nsd_tst= [NSData dataWithBytes:str_tst.c_str() length:str_tst.length()];
	// NSLog(@"%@", [nsd_tst description]);

	std::string str_out;
	str_out.insert(str_out.end(), kvch_F00D_MARKER.begin(), kvch_F00D_MARKER.end());
	str_out+= str_len;
	str_out+= str_msg;
	str_out.insert(str_out.end(), kvch_DEAD_MARKER.begin(), kvch_DEAD_MARKER.end());

	return str_out;
}

//----------------------------------------------------------------------------------------------------------------

// std::vector<char> vch_buffer_;

std::size_t l4end::deframe(std::vector<char>* pvch_buf, const char* kpch_recvd, std::size_t szt_length, std::function<void(std::string)> lf_process_msg)
{
	pvch_buf->insert(pvch_buf->end(), kpch_recvd, kpch_recvd+szt_length);
	
	while (pvch_buf->size() >= kszt_MARKER_SIZE + kszt_LENGTH_FIELD_SIZE + kszt_MARKER_SIZE)
	{
		std::vector<char>::iterator pos= find(pvch_buf->begin(), pvch_buf->end(), (char)0xF0);

		// std::string str_t= std::string(pvch_buf->begin(), pos);
		// std::cout << "DELETED: [" << string_to_hex(str_t) << "]" << std::endl;

		pvch_buf->erase(pvch_buf->begin(), pos);
		if (pos != pvch_buf->begin())
			continue;

		// F00D
		
		const std::vector<char> kvch_F00D(pvch_buf->begin(), pvch_buf->begin()+kszt_MARKER_SIZE);
		if (kvch_F00D != kvch_F00D_MARKER) {
			pvch_buf->erase(pvch_buf->begin());
			continue;
		}

		// ui_len

		const std::vector<char> kvch_len(pvch_buf->begin()+kszt_MARKER_SIZE, pvch_buf->begin()+kszt_MARKER_SIZE+kszt_LENGTH_FIELD_SIZE);

		t_len1s len1s;
		strncpy(len1s._ach4, &kvch_len[0], 4);
		unsigned int ui_msglen= ~len1s._ui32;
		
		// printf("ui_len 1s: %d %x: %x %x %x %x \n", ui_msglen, len1s._ui32, len1s._ach4[3]&0xff, len1s._ach4[2]&0xff, len1s._ach4[1]&0xff, len1s._ach4[0]&0xff );
		
		if (ui_msglen >= kszt_MAX_MSG_SIZE) {
			pvch_buf->erase(pvch_buf->begin());
			continue;
		}

		// DEAD

		const std::size_t kszt_LENGTH_FRAME= kszt_MARKER_SIZE + kszt_LENGTH_FIELD_SIZE + ui_msglen + kszt_MARKER_SIZE;
		assert(kszt_LENGTH_FRAME < kszt_MAX_BUFFER_SIZE);
		
		if (pvch_buf->size() < kszt_LENGTH_FRAME) {
			// std::cerr << "Incomplete message; wait for more data." << std::endl;
			break;
		}

		const std::vector<char> kvch_DEAD(pvch_buf->begin()+kszt_MARKER_SIZE+kszt_LENGTH_FIELD_SIZE+ui_msglen, pvch_buf->begin()+kszt_LENGTH_FRAME);
		if (kvch_DEAD != kvch_DEAD_MARKER) {
			pvch_buf->erase(pvch_buf->begin());
			continue;
		}

		const std::vector<char> kvch_msg(pvch_buf->begin()+kszt_MARKER_SIZE+kszt_LENGTH_FIELD_SIZE, pvch_buf->begin()+kszt_MARKER_SIZE+kszt_LENGTH_FIELD_SIZE+ui_msglen);
		pvch_buf->erase(pvch_buf->begin(), pvch_buf->begin()+kszt_LENGTH_FRAME);

		lf_process_msg( std::string(kvch_msg.begin(), kvch_msg.end()) );
	}
	
	if (pvch_buf->size() > kszt_MAX_BUFFER_SIZE) {
		std::cerr << "Buffer size exceeded the maximum allowed size. Clearing the buffer." << std::endl;
		pvch_buf->clear();
	}

	return pvch_buf->size();
}

//----------------------------------------------------------------------------------------------------------------

//	size_t decode(const char * const kpks_recvd, size_t zt_len, std::string* pstring_msg)
//	{
//		size_t idx= 0;
//		int res= -1;
//
//		short h_hexlen_prefix; res= sscanf(kpks_recvd+idx,"%2hx",&h_hexlen_prefix); idx+= 2;
//		if (res != 1)
//			return -1;
//		if (h_hexlen_prefix+idx > zt_len)
//			return -1;
//
////		pstring_prefix->append(kpks_recvd+idx, h_hexlen_prefix); idx+= h_hexlen_prefix;
////		peeid_sender->copy(kpks_recvd+idx); idx+= peeid_sender->length();
//
//		short h_hexlen_msg; res= sscanf(kpks_recvd+idx,"%4hx",&h_hexlen_msg); idx+= 4;
//		if (res != 1)
//			return -1;
//		if (h_hexlen_msg+idx > zt_len)
//			return -1;
//
//		pstring_msg->append(kpks_recvd+idx, h_hexlen_msg); idx+= h_hexlen_msg;
//
//		return idx;
//	}
//}

